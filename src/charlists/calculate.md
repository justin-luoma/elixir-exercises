
## calculate
Write a function that takes a single-quoted string in the form `number [+-*/] number` and returns the result of the 
calculation.
```elixir
Parse.calculate('123 + 27') # => 150
Parse.calculate('1 - 1') # => 0
Parse.calculate('3 * 3') # => 9
Parse.calculate('10 / 2') # => 5.0
```
