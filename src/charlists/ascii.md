
## ascii

Write a function that returns `true` if a single-quoted string contains only printable ASCII characters. (`space` (`
` or `\s`) through `tilde` (`~`))

```elixir
MyCharlist.is_ascii?('~123$') # => true
MyCharlist.is_ascii?('∂') # => false
```
