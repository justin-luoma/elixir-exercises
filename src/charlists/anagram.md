
## anagram
Write a function that returns `true` is parameters are anagrams
```elixir
Anagram.anagram?("hello", "elloh") # => true
Anagram.anagram?('hello', 'elloh') # => true
Anagram.anagram?("hello", 'world') # => false
```
