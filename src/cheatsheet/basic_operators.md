
# Basic Operators

{{#include operators/arithmetic.md}}

{{#include operators/boolean.md}}

{{#include operators/comparison.md}}

{{#include operators/strings.md}}

{{#include operators/lists.md}}

{{#include operators/sigils.md}}

###

---

[All operators](https://hexdocs.pm/elixir/operators.html)
