
## Atoms
An atom is a constant whose value is its own name. Some other languages call these symbols. They are often useful to
enumerate over distinct values.
```elixir
:apple
:banana

:apple == :apple # => true
```

The booleans `true` and `false` are atoms.
```elixir
true == :true # => true
is_atom(false) # => true
is_boolean(:false) # => true
```

---

[docs](https://hexdocs.pm/elixir/Atom.html)
