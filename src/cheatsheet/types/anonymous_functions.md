
## Anonymous Functions
```elixir
add = fn a, b -> a + b end

add.(5, 2) # => 7

is_function(add) # => true
```

Anonymous functions can also access variables that are in scope when the function is defined.
```elixir
add = fn a, b -> a + b end

double = fn a -> add.(a, a) end
double.(2) # => 4
```
A variable assigned inside a function does not affect its surrounding environment:
```elixir
x = 42

(fn -> x = 0 end).()

x # => 42
```

---

[docs](https://hexdocs.pm/elixir/Function.html)
