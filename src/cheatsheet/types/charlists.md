
## Charlists
**A charlist is a list of integers where all the integers are valid code points.** Whereas double-quotes creates strings, single-quotes create charlist literals:
```elixir
'hello' # => 'hello'
[?h, ?e, ?l, ?l, ?o] # => 'hello'
```
String (binary) concatenation uses the `<>` operator but charlists, being lists, use the list concatenation operator `++`:
```elixir
'this ' <> 'fails'
** (ArgumentError) expected binary argument in <> operator but got: 'this '
    (elixir) lib/kernel.ex:1821: Kernel.wrap_concatenation/3
    (elixir) lib/kernel.ex:1808: Kernel.extract_concatenations/2
    (elixir) expanding macro: Kernel.<>/2
    iex:1: (file)
    
'this ' ++ 'works' # => 'this works'
```
---
