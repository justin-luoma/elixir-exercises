
## Floats
Float numbers require a dot followed by at least one digit and also support `e` for scientific notation:
```elixir
my_float = 1.0
my_float_2 = 1.0e-10
```
Floats are 64-bit double precision (15 decimal places)
```elixir
round(3.58) # => 4
trunc(3.58) # => 3
```

---

[docs](https://hexdocs.pm/elixir/Float.html)
