
## Booleans
Elixir supports `true` and `false` as booleans; everything is truthy except for `false` and `nil`:
```elixir
true # => true
false # => false
```

`and`, `or`, and `not` operate on boolean first arguments only, where `&&`, `||`, and `!` can accept any types
```elixir
true and false # => false
false or true # => true
not false # => true
true and 42 # => 42
```

---
