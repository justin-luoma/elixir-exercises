
## Integers
```elixir
my_int = 255
IO.puts("Integer #{is_integer(my_int)}")
# => "Integer true"
```

Binary, octal, and hexadecimal numbers also supported
```elixir
# binary
0b1000101 # => 69

# octal
0o644 # => 420

# hex
0x29a # => 666
```

---

[docs](https://hexdocs.pm/elixir/Integer.html)
