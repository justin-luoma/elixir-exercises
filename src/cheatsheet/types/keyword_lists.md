
## Keyword Lists
Keyword lists have three special characteristics:

> Keys must be atoms.
> 
> Keys are ordered, as specified by the developer.
> 
> Keys can be given more than once.

```elixir
my_keywords = [{:foo, "bar"}, {:hello, "world"}]
# or
my_keywords = [foo:  "bar", hello: "world"]

[{:trim, true}] == [trim: true] # => true
```
### Usually used for configuration options
```elixir
String.split("1 2 3", " ") # => [1, 2, 3]
```
But when used with multiple spaces:
```elixir
String.split("1   2  3 ", " ") # => ["1", "", "", "2", "", "3", ""]
```
`String.split/3` allows for options to be passed in:
```elixir
String.split("1   2  3 ", " ", [trim: true]) # => [1, 2, 3]
# or
String.split("1   2  3 ", " ", trim: true) # => [1, 2, 3]
```

---

[docs](https://hexdocs.pm/elixir/Keyword.html)
