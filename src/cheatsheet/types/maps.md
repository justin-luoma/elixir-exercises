
## Maps
> Maps allow any value as a key.
>
> Maps’ keys do not follow any ordering.

```elixir
my_map = %{:a => 1, 2 => :b}
map[:a] # => 1
map[2] # => :b
map[:c] # => nil
```

Pattern matching can be used to retrieve values
```elixir
my_map = %{:a => 1, 2 => :b}
%{:a => a} = my_map
a # => 1
```
Variables can also be used
```elixir
n = 1
map = %{n => :one}
map[n] # => :one
```
Atom key maps can use special syntax
```elixir
%{:foo => "bar", :hello => "world"}
# or
%{foo: "bar", hello: "world"}
```

---

[docs](https://hexdocs.pm/elixir/Map.html)
