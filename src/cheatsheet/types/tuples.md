
## Tuples
```elixir
my_tuple = {:ok, "hello"}

my_tuple_2 = {1, "two", :three, false}
```

---

[docs](https://hexdocs.pm/elixir/Tuple.html)
