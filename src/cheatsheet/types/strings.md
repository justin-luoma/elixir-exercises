
## Strings
Strings in Elixir are wrapped in double quotes and UTF-8 encoded:
```elixir
my_string = "world"

IO.puts("Hello, #{my_string}!") # => "Hello, world!"
```

Strings are represented as contiguous sequences of bytes known as binaries:
```elixir
is_binary("hello") # => true
```

---

[docs](https://hexdocs.pm/elixir/String.html)
