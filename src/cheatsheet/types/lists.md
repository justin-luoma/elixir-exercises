
## Lists (Linked Lists)
```elixir
my_list = [1, 2, true, 4]

length(my_list) # => 4
```
Two lists can be concatenated or subtracted
```elixir
[1, 2, 3] ++ [4, 5, 6] # => [1, 2, 3, 4, 5, 6]

[1, true, 2, false, 3, true] -- [true, false] # => [1, 2, 3, true]
```
List operators never modify the existing list. Concatenating to or removing elements from a list returns a new list. Elixir data structures are immutable.

---

[docs](https://hexdocs.pm/elixir/List.html)
