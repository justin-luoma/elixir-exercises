
## Binaries
**A binary is a bitstring where the number of bits is divisible by 8.** That means that every binary is a bitstring, 
but not every bitstring is a binary:
```elixir
is_bitstring(<<3::4>>) # => true
is_binary(<<3::4>>) # => false

is_bitstring(<<0, 255, 42>>) # => true
is_binary(<<0, 255, 42>>) # => true

is_binary(<<42::16>>) # => true
```
Pattern matching can be used:
```elixir
<<0, 1, x>> = <<0, 1, 2>>
x # => 2
```
Note that unless you explicitly use `::` modifiers, each entry in the binary pattern is expected to match a single byte (exactly 8 bits). If we want to match on a binary of unknown size, we can use the `binary` modifier at the end of the pattern:
```elixir
<<0, 1, x::binary>> = <<0, 1, 2, 3>>
x # => <<2, 3>>
```
There are a couple other modifiers that can be useful when doing pattern matches on binaries. The `binary-size(n)` modifier will match `n` bytes in a binary:
```elixir
<<head::binary-size(2), rest::binary>> = <<0, 1, 2, 3>>
head # => <<0, 1>>
rest # => <<2, 3>>
```
**A string is a UTF-8 encoded binary**, where the code point for each character is encoded using 1 to 4 bytes. Thus 
every string is a binary, but due to the UTF-8 standard encoding rules, not every binary is a valid string.
```elixir
is_binary("hello") # => true
is_binary(<<239, 191, 19>>) # => true
String.valid?(<<239, 191, 19>>) # => false
```
The string concatenation operator `<>` is actually a binary concatenation operator:
```elixir
"a" <> "ha" # => "aha"
<<0, 1>> <> <<2, 3>> # => <<0, 1, 2, 3>>
```
You can store integers, floats, and other binaries in binaries:
```elixir
int = << 1 >> # => <<1>>
float = << 2.5 :: float >> # => <<64, 4, 0, 0, 0, 0, 0, 0>>
mix = << int :: binary, float :: binary >> # => <<1, 64, 4, 0, 0, 0, 0, 0, 0>>
```
When in doubt, specify the type of each field, hyphens are used to separate multiple attributes, available types are `binary`, 
`bits`, 
`bitstring`, `bytes`, `float`, 
`integer`, `utf8`, `utf16`, and `utf32`, there are also qualifiers:
 - `size(`n`)`: size of field in bits
 - `signed` or `unsigned`: for integers
 - `big`, `little`, or `native`: endianness
```elixir
<< length::unsigned-integer-size(12), flags::bitstring-size(4) >>  = data
```
### Advanced pattern matching
Similar to list processing, we can use patterns to split head from the rest of the list, but rather than use `[ head | 
tail ]`,
we use `<< 
head::utf8, tail::binary >>`. And rather than terminate when we reach the 
empty list, `[]`, we look for an empty binary, `<<>>`.
```elixir
defmodule Utf8 do
  def each(str, func) when is_binary(str), do: _each(str, func)

  defp _each(<< head :: utf8, tail :: binary >>, func) do
    func.(head)
    _each(tail, func)
  end

  defp _each(<<>>, _func), do: []
end

Utf8.each "∂og", fn char -> IO.puts char end
# => 8706
# 111
# 103
```
An IEEE 754 float has a sign bit, 11 bits of exponent, and 52 bits of mantissa. The exponent is biased by 1023, and the mantissa is a fraction with the top bit assumed to be 1. So we can extract the fields and then use :math.pow, which performs exponentiation, to reassemble the number:
```elixir
<< sign::size(1), exp::size(11), mantissa::size(52) >> = << 3.14159::float >>
(1 + mantissa / :math.pow(2, 52)) * :math.pow(2, exp - 1023) * (1 - 2 * sign) # => 3.14159
```

---
