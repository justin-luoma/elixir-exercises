
## Boolean
`or`, `and`, and `not` operators are strict in the sense that they expect something that evaluates to a boolean (`true` or `false`) as their first argument:
```elixir
true and true
true
false or is_atom(:example)
true
1 and true
```

`or` and `and` are short-circuit operators. They only execute the right side if the left side is not enough to determine the result:
```elixir
false and raise("This error will never be raised") # => false
true or raise("This error will never be raised") # => true
```
For `||`, `&&`, and `!`, all values except `false` and `nil` will evaluate to true:
```elixir
# or
1 || true # => 1
false || 11 # => 11

# and
nil && 13 # => nil
true && 17 # => 17

# not
!true # => false
!1 # => false
!nil # => true
```
