
## Arithmetic
Order of operators = `BEDMAS`: brackets, exponent, division, multiplication, addition, substraction
```elixir
2 + 1 # => 3
3 - 1 # => 2
2 * 4 # => 8
10 / 5 # => 2.0
```
Integer division
```elixir
div(10, 2) # => 5
rem(10, 3) # => 1
```
