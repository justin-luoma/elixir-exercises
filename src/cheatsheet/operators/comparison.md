
## Comparison
`==`, `!=`, `===`, `!==`, `<=`, `>=`, `<`, and `>`:
```elixir
1 > 2 # => false
1 != 2 # => true
2 == 2 # => true
2 <= 3 # => true
```
For strict comparison, use `===`:
```elixir
2 == 2.0 # => true
2 === 2.0 # => false
```
Data Type Comparison
```elixir
1 < :atom # => true
```
Pragmatism allows comparison of types. Sorting order:
```
number < atom < reference < function < port < pid < tuple < map < list < bitstring
```
