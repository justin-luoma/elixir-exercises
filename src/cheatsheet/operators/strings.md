
## Strings
Concatenation:
```elixir
name = "Bob"

"Hello, " <> name # => "Hello, Bob"
```
Interpolation:
```elixir
name = "Bob"

"Hello, #{name}" # => "Hello, Bob"
```
