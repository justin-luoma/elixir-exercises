
## Sigils

Sigils provide an alternative syntax for working with literals.

Builtin sigils:

- `~c` Character list **with** escaping and interpolation
- `~C` Character list **without** escaping or interpolation
- `~r` Regular expression **with** escaping and interpolation
- `~R` Regular expression **without** escaping or interpolation
- `~s` String **with** escaping and interpolation
- `~S` String **without** escaping or interpolation
- `~w` Word list **with** escaping and interpolation
- `~W` Word list **without** escaping or interpolation
- `~N` `NativeDateTime`[^ndt] struct
- `~U` `DateTime`[^dt] struct

The following delimiters are supported:

`< >`, `{ }`, `[ ]`, `( )`, `| |`, `/ /`, `" "`, `' '`

### Char List 
`~c`/`~C`

```elixir
~c/2 + 7 = #{2 + 7}/
# => '2 + 7 = 9'

~C/2 + 7 = #{2 + 7}/
# => '2 + 7 = \#{2 + 7}'
```

### Regular Expression 
`~r`/`~R`

```elixir
re = ~r/elixir/

"Elixir" =~ re
# => false

"elixir" =~ re
# => true
```

### String 
`~s`/`~S`

```elixir
~s/the cat in the hat on the mat/
# => "the cat in the hat on the mat"

~S/the cat in the hat on the mat/
# => "the cat in the hat on the mat"
```

### Word List 
`~w`/`~W`

```elixir
~w/i love elixir/
#=> ["i", "love", "elixir"]

~W/i love elixir/
# => ["i", "love", "elixir"]

~w/i love #{'e'}lixir school/
# => ["i", "love", "elixir", "school"]

~W/i love #{'e'}lixir school/
# => ["i", "love", "\#{'e'}lixir", "school"]
```

### NativeDateTime 
`~N`

```elixir
{:ok, ~N[2015-01-23 23:50:07]} == NaiveDateTime.from_iso8601("2015-01-23 23:50:07")
```

### DateTime 
`~U`

```elixir
{:ok, ~U[2015-01-23 23:50:07Z], 0} == DateTime.from_iso8601("2015-01-23 23:50:07Z")

{:ok, ~U[2015-01-24 05:50:07Z], -21600} == DateTime.from_iso8601("2015-01-23 23:50:07-0600")
```

### Creating a Sigil

```elixir
defmodule MySigils do

  def sigil_p(string, []), do: String.upcase(string)

end

import MySigils

~p/elixir/
# => ELIXIR
```

[^ndt]: [NativeDateTime](https://hexdocs.pm/elixir/NaiveDateTime.html)

[^dt]: [DateTime](https://hexdocs.pm/elixir/DateTime.html)
