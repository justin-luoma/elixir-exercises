# with

### Converting a `case` to `with`

```elixir
msg = case read_line(socket) do
        {:ok, data} ->
          case KVServer.Command.parse(data) do
            {:ok, command} ->
              KVServer.Command.run(command)

            {:error, _} = err ->
              err
          end

        {:error, _} = err ->
          err
      end
```

```elixir
msg =
      with {:ok, data} <- read_line(socket),
           {:ok, command} <- KVServer.Command.parse(data),
           do: KVServer.Command.run(command)
```
