
# Data Types

{{#include types/integers.md}}
{{#include types/floats.md}}
{{#include types/booleans.md}}
{{#include types/atoms.md}}
{{#include types/strings.md}}
{{#include types/anonymous_functions.md}}
{{#include types/lists.md}}
{{#include types/tuples.md}}
{{#include types/keyword_lists.md}}
{{#include types/maps.md}}
{{#include types/binaries.md}}
{{#include types/charlists.md}}
