# Tasks

### async/await

```elixir
task = Task.async(fn -> compute_something_expensive() end)
res  = compute_something_else()
res + Task.await(task)
```

### run a task on a different node

the following is in our supervisor tree

```elixir
{Task.Supervisor, name: KV.RouterTasks}
```

we have nodes running on `foo@computer_name` and `bar@computer_name`[^task]:

```elixir
task = Task.Supervisor.async({KV.RouterTasks, :"foo@computer_name"}, fn -> 
  {:ok, node()} 
end)
Task.await(task) # => {:ok, :"foo@computer_name"}
```

the preferred way to do this is by specifying the module, function, and arguments explicitly:

```elixir
task = Task.Supervisor.async({KV.RouterTasks, :"foo@computer-name"}, Kernel, :node, [])
Task.await(task) # => :"foo@computer_name" 
```

[^task]: this will only work if the target node has exactly the same code version as the caller
