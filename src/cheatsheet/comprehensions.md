
# Comprehensions

Comprehensions are syntactic sugar for looping over Enumerables

```elixir
for n <- [1, 2, 3, 4], do: n * n
# => [1, 4, 9, 16]

list = [1, 2, 3, 4]
for n <- list, do: n * n
# => [1, 4, 9, 16]

for n <- 1..4, do: n * n
# => [1, 4, 9, 16]
```

More Enumerables
```elixir
# Keyword Lists
for {_key, val} <- [one: 1, two: 2, three: 3], do: val
# => [1, 2, 3]

# Maps
for {k, v} <- %{"a" => "A", "b" => "B"}, do: {k, v}
# => [{"a", "A"}, {"b", "B"}]

# Binaries
for <<c <- "hello">>, do: <<c>>
# => ["h", "e", "l", "l", "o"]
```

Pattern matching
```elixir
for {:ok, val} <- [ok: "Hello", error: "Unknown", ok: "World"], do: val
# => ["Hello", "World"]
```

Multiple generators
```elixir
list = [1, 2, 3, 4]
for n <- list, times <- 1..n do
  String.duplicate("*", times)
end
# => ["*", "*", "**", "*", "**", "***", "*", "**", "***", "****"]
```
### Filters
Using `Integer.is_even/1` and `Integer.rem/2` to filter values
```elixir
import Integer

for x <- 1..10, is_even(x), do: x
# => [2, 4, 6, 8, 10]

for x <- 1..100,
  is_even(x),
  rem(x, 3) == 0, do: x
# => [6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96]
```
### :into
Create collectables other than lists with `:into`
```elixir
# Maps
for {k, v} <- [one: 1, two: 2, three: 3], into: %{}, do: {k, v}
# => %{one: 1, three: 3, two: 2}

# Binary
for c <- [72, 101, 108, 108, 111], into: "", do: <<c>>
# => "Hello"
```
