
## capitalize
Write a function that capitalizes sentences in a string.
```elixir
Capitalize.capitalize_sentences "oh. a DOG. woof. "    
# => "Oh. A dog. Woof. "
```
