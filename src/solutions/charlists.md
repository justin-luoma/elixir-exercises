# Charlist Solutions

{{#include charlists/ascii.md}}

{{#include charlists/anagram.md}}

{{#include charlists/calculate.md}}

---

[exercises](../charlists.md)
