
## center solution
```elixir
defmodule Center do
  defp _center([], list,_), do: Enum.each(list, &(IO.puts(&1)))

  defp _center([head | tail], list, max) do
    if String.length(head) == max do
      _center(tail, list ++ [head], max)
    else
      len = String.length(head)
      diff = max - len
      new_length = len + div(diff, 2)
      _center(tail, list ++ [String.pad_leading(head, new_length)], max)
    end
  end

  defp max_length(list), do: String.length(Enum.max_by list, fn s -> String.length(s) end)

  def center(list), do: _center(list, [], max_length(list))
end
```
