
## capitalize solution
```elixir
defmodule Capitalize do
  def capitalize_sentences(<<head::utf8, tail::binary>>) do
    _capitalize_sentences(tail, String.capitalize(<<head>>))
  end

  defp _capitalize_sentences(<<>>, str), do: str
  defp _capitalize_sentences(<<head::utf8, tail::binary>>, str) do
    if <<head>> == "." && (String.length(tail) > 2) do
      <<s_1::utf8, s_2::utf8, tail::binary>> = tail
      _capitalize_sentences(tail, str <> <<head>> <> <<s_1>> <> String.capitalize(<<s_2>>))
    else
      _capitalize_sentences(tail, str <> String.downcase(<<head>>))
    end
  end
end
```
