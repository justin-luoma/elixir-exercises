## calculate solution

```elixir
defmodule Parse do
  def calculate(str), do: _calculate(str, 0, nil, 0)

  defp _calculate([], n_1, func, n_2), do: func.(n_1, n_2)

  defp _calculate([digit | tail], n_1, nil, n_2)
       when digit in '0123456789',
       do: _calculate(tail, n_1 * 10 + digit - ?0, nil, n_2)

  defp _calculate([digit | tail], n_1, func, n_2)
       when digit in '0123456789',
       do: _calculate(tail, n_1, func, n_2 * 10 + digit - ?0)

  defp _calculate([ ?\s | tail], n_1, nil, n_2), do: _calculate(tail, n_1, nil, n_2)
  defp _calculate([ ?\s | tail], n_1, func, n_2), do: _calculate(tail, n_1, func, n_2)

  defp _calculate([ ?+ | tail], n_1, nil, n_2), do: _calculate(tail, n_1, &(&1 + &2), n_2)
  defp _calculate([ ?- | tail], n_1, nil, n_2), do: _calculate(tail, n_1, &(&1 - &2), n_2)
  defp _calculate([ ?* | tail], n_1, nil, n_2), do: _calculate(tail, n_1, &(&1 * &2), n_2)
  defp _calculate([ ?/ | tail], n_1, nil, n_2), do: _calculate(tail, n_1, &(&1 / &2), n_2)
end
```
