
## anagram solution
```elixir
defmodule Anagram do
  def anagram?(word1, word2) when is_list(word2), do: to_charlist(word1) == Enum.reverse(word2)
  def anagram?(word1, word2) when is_binary(word2), do: to_charlist(word1) == Enum.reverse(to_charlist(word2))
end
```
