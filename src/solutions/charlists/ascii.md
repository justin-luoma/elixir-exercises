
## ascii solution

```elixir
defmodule MyCharlist do
  def is_ascii?([head | []]), do: _is_ascii?(head)
  def is_ascii?([head | tail]) do
    if _is_ascii?(head) do
      is_ascii?(tail)
    else
      false
    end
  end

  defp _is_ascii?(v), do: v in Enum.to_list(?\s..?~)
end
```
