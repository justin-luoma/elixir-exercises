## average solution

```elixir
defmodule Maps do
  def average(%{"age" => age1}, %{"age" => age2}), do: (age1 + age2) / 2
end
```

