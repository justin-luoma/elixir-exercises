
## caesar solution

```elixir
defmodule MyList do
  defp _caesar([], _, out), do: out
  defp _caesar([head | tail], n, out) when head + n <= 122, do: _caesar(tail, n, out ++ [head + n])
  defp _caesar([head | tail], n, out) when head + n > 122 do
    tmp = head + n - 122
    _caesar(tail, n, out ++ [96 + tmp])
  end

  def caesar(word, n), do: _caesar(word, n, '')
end
```
