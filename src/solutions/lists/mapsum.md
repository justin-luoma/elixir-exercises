
## mapsum solution

```elixir
defmodule MyList do
  defp _mapsum([], _, val), do: val
  defp _mapsum([head | tail], func, val) do
    tmp = func.(head)
    _mapsum(tail, func, val + tmp)
  end

  def mapsum(list, func) do
    _mapsum(list, func, 0)
  end
end
```
