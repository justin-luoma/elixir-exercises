
## max solution

```elixir
defmodule MyList do
  defp _max([], {max}), do: max
  defp _max([head | tail], max) when {head} > max, do: _max(tail, {head})
  defp _max([head | tail], max) when {head} < max, do: _max(tail, max)

  def max(list), do: _max(list, {})
end
```
