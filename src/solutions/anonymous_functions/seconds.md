
## seconds solution
```elixir
defmodule Functions do
  def seconds(), do: fn sec -> sec * 60 * 60 * 24 end
end
```
