
## convert solution

```elixir
defmodule Functions do
  def convert(:fahrenheit), do: fn c -> c * (9/5) + 32 end
  def convert(:celcius), do: fn f -> (f - 32) * 5/9 end
end
```
