# List Solutions

{{#include lists/concat.md}}

{{#include lists/reduce.md}}

{{#include lists/mapsum.md}}

{{#include lists/max.md}}

{{#include lists/caesar.md}}

{{#include lists/span.md}}

---

[exercises](../lists.md)
