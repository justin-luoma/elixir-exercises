
## average

Write a function that takes two maps with `"age"` keys and return the average.

```elixir
Maps.average(%{"age" => 30}, %{"age" => 60, "name" => "Bob"})
# => 45.0

one = %{ "age" => 30, "name" => "Bob"}
two = %{ "age" => 15, "name" => "Bill"}
Maps.average(one, two)
# => 22.5
```
