
# Anonymous Function Exercises

{{#include anonymous_functions/seconds.md}}

{{#include anonymous_functions/convert.md}}

---

[solutions](solutions/anonymous_functions.md)
