# Charlist Exercises

{{#include ./charlists/ascii.md}}

{{#include ./charlists/anagram.md}}

{{#include ./charlists/calculate.md}}

---

[solutions](solutions/charlists.md)
