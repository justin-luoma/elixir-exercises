# Elixir Exercises

## [Exercises](exercises.md)

## [Cheatsheet](cheatsheet/cheatsheet.md)

## Links

* ### General
    * [elixir-lang.org](https://elixir-lang.org/)
    * [library docs](https://hexdocs.pm/elixir/Kernel.html)
    * [Elixir School](https://elixirschool.com/en/)
    * [elixir examples](https://elixir-examples.github.io/)
    * [elixir by example](https://elixirbyexample.org/)
* ### Distributed Tasks
    * [Elixir-lang guide](https://elixir-lang.org/getting-started/mix-otp/distributed-tasks.html)
    * [Distribunomicon](https://learnyousomeerlang.com/distribunomicon)
    * [Phoenix PubSub](https://github.com/phoenixframework/phoenix_pubsub)

## Helpful Libraries

* [StreamData](https://github.com/whatyouhide/stream_data): data generation and property-based testing
* [ExCoveralls](https://github.com/parroty/excoveralls): test coverage
