## span

Write a function that takes two parameters: `from` and `to`, and returns a list of numbers from `from` to `to`.

```elixir
MyList.span(1, 3)
# => [1, 2, 3]
```
