## reduce

Write a reduce function

```elixir
MyList.reduce([1, 2, 3], 0, fn val, acc -> val + acc end)
# => 6
```
