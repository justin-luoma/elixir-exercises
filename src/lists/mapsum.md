## mapsum

Write a function that takes a list and a function. It applies the function to each element of the list and then sums
the results

```elixir
MyList.mapsum([1, 2, 3], &(&1 * &1))
# => 14
```
