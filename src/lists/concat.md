## concat

Write a function that concatenates two lists

```elixir
MyList.concat([1, 2, 3], [4, 5, 6])
# => [1, 2, 3, 4, 5, 6]
```
