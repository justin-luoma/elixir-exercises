# Summary

[Introduction](README.md)

---

[Exercises](exercises.md)

- [Lists](lists.md)
  - [concat](lists/concat.md)
  - [reduce](lists/reduce.md)
  - [mapsum](lists/mapsum.md)
  - [max](lists/max.md)
  - [caesar](lists/caesar.md)
  - [span](lists/span.md)

- [Charlists](charlists.md)
  - [ascii](charlists/ascii.md)
  - [anagram](charlists/anagram.md)
  - [calculate](charlists/calculate.md)

- [Strings](strings.md)
  - [center](strings/center.md)
  - [capitalize](strings/capitalize.md)

- [Maps](maps.md)
  - [average](maps/average.md)

- [Anonymous Functions](anonymous_functions.md)
  - [seconds](anonymous_functions/seconds.md)
  - [convert](anonymous_functions/convert.md)

-----------

# Solutions

- [Lists](solutions/lists.md)
  - [concat](solutions/lists/concat.md)
  - [reduce](solutions/lists/reduce.md)
  - [mapsum](solutions/lists/mapsum.md)
  - [max](solutions/lists/max.md)
  - [caesar](solutions/lists/caesar.md)
  - [span](solutions/lists/span.md)

- [Charlists](solutions/charlists.md)
  - [ascii](solutions/charlists/ascii.md)
  - [anagram](solutions/charlists/anagram.md)
  - [calculate](solutions/charlists/calculate.md)

- [Strings](solutions/strings.md)
  - [center](solutions/strings/center.md)
  - [capitalize](solutions/strings/capitalize.md)

- [Maps](solutions/maps.md)
  - [average](solutions/maps/average.md)

- [Anonymous Functions](solutions/anonymous_functions.md)
    - [seconds](solutions/anonymous_functions/seconds.md)
    - [convert](solutions/anonymous_functions/convert.md)

---

# Reference

- [Basic Types](cheatsheet/basic_types.md)
  - [Integers](cheatsheet/types/integers.md)
  - [Floats](cheatsheet/types/floats.md)
  - [Booleans](cheatsheet/types/booleans.md)
  - [Atoms](cheatsheet/types/atoms.md)
  - [Strings](cheatsheet/types/strings.md)
  - [Anonymous Functions](cheatsheet/types/anonymous_functions.md)
  - [Lists](cheatsheet/types/lists.md)
  - [Tuples](cheatsheet/types/tuples.md)
  - [Keyword Lists](cheatsheet/types/keyword_lists.md)
  - [Maps](cheatsheet/types/maps.md)
  - [Binaries](cheatsheet/types/binaries.md)
  - [Charlists](cheatsheet/types/charlists.md)

- [Basic Operators](cheatsheet/basic_operators.md)
  - [Arithmetic](cheatsheet/operators/arithmetic.md)
  - [Boolean](cheatsheet/operators/boolean.md)
  - [Comparison](cheatsheet/operators/comparison.md)
  - [Strings](cheatsheet/operators/strings.md)
  - [Lists](cheatsheet/operators/lists.md)
  - [Sigils](cheatsheet/operators/sigils.md)

- [Comprehensions](cheatsheet/comprehensions.md)

[Cheatsheet](cheatsheet/cheatsheet.md)
