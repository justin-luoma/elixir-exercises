
## seconds

Write a function that returns an anonymous function that takes a number of days and returns the number of seconds in 
the specified number of days.

```elixir
seconds = Functions.seconds

seconds.(2) # => 172800
seconds.(1) # => 86400
```
