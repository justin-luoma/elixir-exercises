
## convert

Write a function that takes either `:fahrenheit` or `:celsius` and returns an anonymous function that converts TO 
the passed in value.

```elixir
c_to_f = Functions.convert(:fahenheit)
f_to_c = Functions.convert(:celcius)

c_to_f.(0) # => 32.0
c_to_f.(100) # => 212.0

f_to_c.(32) # => 0.0
f_to_c.(212) # => 100.0
```
